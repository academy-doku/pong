package model;

public class Model {

    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;
    public static int scoreP1 = 0;
    public static int scoreP2 = 0;
    public static double playerTwoYPos = HEIGHT / 2;
    public static double playerTwoXPos = WIDTH - 15;
    public static boolean gameStarted;

    private Ball ball;
    private Paddle paddle;
    public Model() {

        paddle = new Paddle(15, 100);

        ball = new Ball(WIDTH / 2, HEIGHT / 2, 5, 5);
    }

    public void update(long elapsedTime) {

        if (gameStarted) {
            ball.update(elapsedTime);

            if (((ball.getX() + 30 > playerTwoXPos) && ball.getY() >= playerTwoYPos && ball.getY() <= playerTwoYPos + 100) ||
                    ((ball.getX() < paddle.getX() + 20) && ball.getY() >= paddle.getY() && ball.getY() <= paddle.getY() + 100)) {
                ball.setBallXSpeed(-ball.getBallXSpeed());
            }
        }
    }

    public Ball getBall() {
        return ball;
    }

    public static boolean isGameStarted() {
        gameStarted = true;
        return gameStarted;
    }

    public static int increaseScoreP1() {
        scoreP1++;
        return scoreP1;
    }

    public static int increaseScoreP2() {
        scoreP2++;
        return scoreP2;
    }

    public Paddle getPaddle() {
        return paddle;
    }
}
