import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import model.Model;

import static model.Model.*;

public class Graphics {

    private Model model;
    private GraphicsContext gc;

    ImagePattern paddle = new ImagePattern(new Image(getClass().getResourceAsStream("/images/bat.png")));

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {
        if (gameStarted) {

            gc.setFill(Color.DARKGREEN);
            gc.fillRect(0, 0, WIDTH, HEIGHT);
            gc.setFill(Color.DARKOLIVEGREEN);
            gc.fillRect(WIDTH / 2, 0, 5, 1000);
            gc.setFill(Color.GOLDENROD);
            gc.fillOval(model.getBall().getX(), model.getBall().getY(), model.getBall().getBall_width(), model.getBall().getBall_height());
            gc.setFill(Color.GOLDENROD);
            gc.fillText("\t\t\t\t\t\t" + scoreP1 + "\t\t\t\t\t\t" + scoreP2, WIDTH / 2 - 200, 100);
            gc.setFill(paddle);
            gc.fillRect(playerTwoXPos, playerTwoYPos, 15, 100);
            gc.setFill(paddle);
            gc.fillRect(model.getPaddle().getX(), model.getPaddle().getY(), model.getPaddle().getwidth(), model.getPaddle().getHeight());
        } else {
            gc.setFill(Color.FORESTGREEN);
            gc.fillRect(0, 0, WIDTH, HEIGHT);
            gc.setFont(Font.font(35));
            gc.setStroke(Color.GOLDENROD);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.strokeText("Click to start...", WIDTH / 2, HEIGHT / 2);

        }


    }
}
