import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import model.Model;

public class InputHandler {
    private Model model;


    public InputHandler(Model model) {
        this.model = model;
    }

    public void onMouseClicked(MouseButton key) {
        if (key == MouseButton.PRIMARY) {
            Model.isGameStarted();
        }
    }

    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.UP) {
            model.getPaddle().moveUP(40);

        } else if (key == KeyCode.DOWN) {
            model.getPaddle().moveDOWN(40);
        }
    }
}
